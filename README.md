# Run MAI locally through Docker
1. Install Docker on your machine
2. Clone this GitLab repo
3. Build a Docker image using the Dockerfile provided in this repo

# Build the Docker image using linux
The following command builds the image using the file "Dockerfile" and names the image "impute-api"
(Note this may take a little while)
```linux
docker build . -f Dockerfile -t impute-api
```
# Run the image inside a Docker container
The following command opens port 80 on your computer and connects to port 80 on the container
(Note -rm automatically removes the container on exit)
```linux
docker run --rm --detach -p 80:80 impute-api
```

# Stop the Docker container
You can find your container id by using the command
```linux
docker ps
```
and the following command stops the container
```linux
docker stop <container_id>
```
 
 4. After successfully running the Docker container, check out the Tutorials available on this repo to learn how to send POST requests to the API in Julia, Python and R