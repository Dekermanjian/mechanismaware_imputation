# Get r from dockerhub
FROM rocker/r-ver:4.0.3

# Get linux libraries that plumber requires
RUN apt-get update -qq && apt-get install -y \
    git-core \
    libssl-dev \
    libcurl4-gnutls-dev

# Make an app directory
RUN mkdir -p ~/myapp

# Copy files in current working directory to app directory
COPY "/" "myapp/"

# Set working directory to app directory
WORKDIR "myapp/"

# Open port 80 (the port plumber is running on)
EXPOSE 80

# install required R packages
RUN R -e "install.packages('BiocManager'); BiocManager::install('pcaMethods'); BiocManager::install('impute'); install.packages(c('plumber', 'dplyr', 'missForest', 'imputeLCMD', 'INDperform', 'caret', 'parallel', 'doParallel', 'randomForest', 'e1071', 'future.apply'))"


# Command to perform when the image is launched into a container
ENTRYPOINT ["Rscript", "launchAPI.R"]
